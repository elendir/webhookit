#!/bin/bash

eval `ssh-agent`;
ssh-add deploy.openssh;

git -C /var/www/dankenservis fetch --all;
git -C /var/www/dankenservis checkout --force origin/master;

composer self-update;
composer install --working-dir /var/www/dankenservis;

rm -r /var/www/dankenservis/temp/cache;