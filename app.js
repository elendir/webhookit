#!/usr/bin/env node

var express = require('express');
var bodyParser = require('body-parser');
var optimist = require('optimist');

var config = require('./app/config');
var receiver = require('./app/receiver');

var argv = optimist.argv;
receiver.setConfig(config.parse(argv.config));

var app = express();
var router = express.Router();

app.use(bodyParser.json());

router.get('/', function(req, res) {
	res.send('hello world');
});

router.post('/', receiver.receive);
app.use('*', router);


// var server = app.listen(3000, '0.0.0.0', function() {
var server = app.listen(3000, function() {
	console.log('Webhookit started!', server.address());
});