var config = {};

exports.setConfig = function(conf) {
	config = conf;
};

exports.receive = function(req, res) {
	config.receivers.forEach(function(receiver) {
		if (receiver.type == 'gitlab') {
			try {
				var repository = req.body.project.name;
        var ref = req.body.ref;

			} catch (exception) {
				console.log('Not using ' + receiver.repo);
			}

			if (receiver.repo == repository && receiver.ref == ref) {
				console.log('Using ' + receiver.repo + ', running ' + receiver.sh);

				var exec = require('child_process').exec;
				exec('sh -x ' + receiver.sh, {timeout: 1000 * 60 * 2}, function(err, stdout, stderr) {
					if (err) {
						console.error(err);
						return;
					}
					console.log('stdout', stdout);
					console.log('stderr', stderr);
				});
			}
		}
	});

	res.send('POST received baby!');
};


