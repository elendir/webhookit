
var fs = require('fs');
var yaml = require('js-yaml');

exports.parse = function(file) {
	var config = yaml.safeLoad(fs.readFileSync(file, 'utf8'));
	console.log('Loaded config', config);
	return config;
};


